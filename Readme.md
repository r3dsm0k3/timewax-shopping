# motivation

Build a shopping cart application

# Running

`git clone the repo`

`cd shopping-cart && npm i `

> The project makes use of the nodemon tool, so it would be convenient if you have `nodemon` as a global package.

> There is also a remote debugger exposed via the port 5858 in case you want to put break points and debug from chrome dev tools or VSCode. 

`nodemon` (assuming you have nodemon installed globally)

This will look for the `nodemon.json` file in the folder and executes the commands specified in it.


# Features

## development
* Well architectured
	* Domain-driven design concepts implemented.
	* Highly decoupled components
	* Composite oriented programming
	* Repository pattern.
	* SOLID and Onion Concepts implemented
	* Dependency Injection.
	* Cleanly seperated api, domain and infrastructure.
	* Want to replace the InMemoryDb with an actual Db? Sure, easily doable with **0 lines of code** to Domain layer.
	* CQRS Ready (There is event/command bus implemented)
* Inversion of Control container with InversifyJS
* nodemon for running dev
* Provided decoupled bootstrap data for coupons/items


### API

The api is exposed via a REST interface and the server is listening (and exposed) from the port **1337**.

#### Create cart
``` sh
curl -X POST \
  http://localhost:1337/cart/ \
  -H 'Host: localhost:1337'
```
The `response.data.identifier` should be treated as **cart_id** from now on.

#### Add item to cart
> Items are bootstraped in the `/src/infrastructure/ItemTypes.ts`
> 
```
curl -X POST \
  http://localhost:1337/cart/{{cart_id}}/item \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:1337' \
  -d '{
	"item_identifier" : "1",
	"quantity" : 1 <- quantity can be more
}'
```

#### Delete items from cart

```
curl -X DELETE \
  http://localhost:1337/cart/{{cart_id}}/item \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:1337' \
  -d '{
	"item_identifier" : "1",
	"quantity" : 1 <- quantity can be more here too
}'
```

#### Get details of cart by identifier

```
curl -X GET http://localhost:1337/cart/{{cart_id}}
```

#### Get all carts in the system (debugging)
```
curl -X GET http://localhost:1337/cart
```

#### Apply coupon code to cart
```
curl -X PUT \
  http://localhost:1337/cart/{{cart_id}}/coupon \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:1337' \
  -d '{
	"coupon_code" : "TimeWax1"
}'
```

#### Reset cart
```
curl -X DELETE http://localhost:1337/cart/{{cart_id}}
```
#### Checkout cart
```
curl -X POST http://localhost:1337/cart/{{cart_id}}/checkout
```

### Architecture & Concepts

The app has been built with adhering to Domain-driven design architecture in mind. It also makes use of CQRS to an extend as well. This helps to decouple and greatly simplify applications, especially as it grow larger and complex.

Domain driven design (DDD) is an approach to software design that values simplicity and modeling code as closely to the business domain as possible. This results in code that can be easily understood by the business and evolved as the needs of the business domain change.

By isolating domain code away from all other concerns of the system like infrastructure, security, transportation, serialization etc; the complexity of the system grows only as large as the complexity of the business or problem domain itself.

If you are new to the concepts of DDD, it's highly recommended to do some background reading to grasp the motivations for this approach.

The app uses dependency injection and inversion of control container using the `inversify` library. You can find the dependency bindings in the `module.ts` file the respective folders for application, api and infrastructure. 

## Layers

The app also layers code using the onion architecture approach. The layers can be found in the `/src` folder


#### Domain.
  The domain layer sits at the centre of the system. It contains domain objects and domain services. This layer has no technical concerns like infrastructure, authentication, data access etc. The goal of the domain layer is to have a place in your application where code can be writen that models your business domains and rules in a way where those business complexities are kept separate from the rest of your application.

As a result, much of the code that gets written in this layer can be read by non-technical staff meaning that greater collaboration with the domain experts and validation of expected behaviours can be performed. Code here is easily unit testable and isolated from the rest of the application.

The domain layer is composed of one or more domains. Domains are logical boundaries around broad groups of related logic. Each domain is comprised of multiple aggregates, which are clusters of closely related data that model a single entity in the real world. Each aggregate has a root, that represents the single point of access into an aggregate and hosts all the actions that can be performed.

An example would be the _Cart_ aggregateroot in the system. "Cart" domain is established to encapsulate all aspects of cart, creation, updation, add/remove items, apply coupon code etc.

We can model that these actions have occured using bus Events. Here are the events for those actions:

`CartUpdated`
`CouponCodeApplied`

whenever the actions are performed on the aggregate root, these events above are broadcasted to the rest of your system, normally with a message bus, each time one of the actions are performed on the aggregate root.
This approach to modeling the business domain is well documented. It's clear what actions a user can perform, what the business rules are those actions, and what data updates as a result.

Each time an action method is called on a domain object, an event is prepared and applied to the when() protected method. This method does a number of things:

* It adds the event into the list of new changes made to the aggregate
* It increments the verison of the aggregate as data has now changed
* It invokes the method named when<EventName> on the aggregate (eg: whenCartUpdated)


At this point it's important to note that the aggregate has not been persisted nor the event published to the bus. This will be the responsibility of the application server/infrastructure that initially invoked the domain action.

#### Application.

The application layer sits around the domain layer. It provides services that act as a gateway to performing actions against the domain. Broadly speaking, these services typically offer one method per command, and can retrieve domain objects from persistence (infrastructure), query other necessary data inputs, gather dependencies to inject and persist data back to the database. Here Application layer is also responsible for handling the CQRS commands and events. Each command needs to have a handler and it should only do one thing and do it good. 


#### API

The api layer, as you would expect does the api hadndling. All the HTTP server and routes are configured here. The http actions sends the command to be handled to the Application layer, and the application layer then mutates/reads domain objects and returns it back to the api. The input validations should be done here to ensure the the values that is given to the inner layers are structurally correct.

#### Infrastructure

Infrasture layer is an outer layer which deals with system, databases, external services, networking, storage in general etc. The infrastructure layer is how the data that is initially held in domain entities (in memory) is persisted in databases or another persistent store. In this app, the actual implementation of the repositories defined in the Domain layer (as the InMemoryDb) is done here. Infrastrucutre does not have any concerns of domain or any other layer. IMO infrastructure is only a necessary evil, it can be really dumb (or even smart. However, its not really important to invest efforts in infrastructure since it not permanent). After all it is cheap,and can be replaced by a newer infrastucture without any changes to the domain or other layers. In this app, the infrastructure layer will also emit the events stored in the domain aggregates after having persisted to the db. 


## TODO
* Unit & Integration Tests
* Docker

