import { BaseDomainException } from './BaseDomainException'
class InvalidDataException extends BaseDomainException {
  constructor(message) {
    super(message)
  }
  public static dueTo(reason: string) {
    return new this(reason)
  }

}
export { InvalidDataException }
