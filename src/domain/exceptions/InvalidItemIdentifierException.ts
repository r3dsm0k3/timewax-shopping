import { BaseDomainException } from './BaseDomainException'
class InvalidItemIdentifierException extends BaseDomainException {
  constructor(message) {
    super(message)
  }
  public static with(reference: string) {
    return new this(`Got invalid item identifier - ${reference}`)
  }
}
export { InvalidItemIdentifierException }
