import { BaseDomainException } from './BaseDomainException'
class InvalidCartIdentifierException extends BaseDomainException {
  constructor(message) {
    super(message)
  }
  public static with(reference: string) {
    return new this(`Got invalid cart identifier - ${reference}`)
  }
}
export { InvalidCartIdentifierException }
