import * as _ from 'lodash'
class BaseDomainException extends TypeError {
  errorDetails: any
  constructor(message, errorDetails = {}) {
    super(message)
    this.errorDetails = errorDetails
    Object.setPrototypeOf(this, new.target.prototype)
    this.name = new.target.name
  }
  toJSONString() {
    if (_.isEmpty(this.errorDetails)) {
      return JSON.stringify({ message: this.message, details: this.errorDetails })
    }
    return JSON.stringify(this.errorDetails)
  }
}
export { BaseDomainException }
