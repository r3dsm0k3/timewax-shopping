import { AggregateRoot } from '@node-ts/ddd'
import * as _ from 'lodash'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { CartItem } from '@domain/value-objects/CartItem'
import { CouponCode } from '@domain/value-objects/CouponCode'
import { InvalidDataException } from '@domain/exceptions/InvalidDataException'
import { Item } from '@domain/value-objects/Item'
import { CartUpdated } from '@domain/events/CartUpdated'
import { CouponCodeApplied } from '@domain/events/CouponCodeApplied'
import { CheckoutPerformed } from '@domain/events/CheckoutPerformed'
import * as uuid from 'uuid'
class Cart extends AggregateRoot {
  private constructor(
        private _identifier: CartIdentifier,
        private _items: CartItem[],
        private _couponCode: CouponCode | null,
        private _createdAt: Date,
        private _lastUpdatedAt: Date,
        private _frozen: boolean
    ) {
    super(_identifier.value)
  }
  public static with(
        identifier: CartIdentifier,
        items: CartItem[],
        couponCode: CouponCode,
        createdAt: Date,
        lastUpdatedAt: Date,
        frozen: boolean
    ) {
    const instance = new this(
            identifier,
            items,
            couponCode,
            createdAt,
            lastUpdatedAt,
            frozen
        )
    return instance
  }
  public static create(
    ): Cart {

    const identifier =  CartIdentifier.with(uuid.v4())
    const frozen = false
    const createdAt = new Date()
    const lastUpdatedAt = createdAt
    const instance = new this(
            identifier,
            [],
            null,
            createdAt,
            lastUpdatedAt,
            frozen
        )
    instance.when(
            new CartUpdated(
                identifier,
                [],
                null,
                createdAt,
                lastUpdatedAt,
                frozen
            )
        )
    return instance
  }
    // getters
  public get identifier(): CartIdentifier {
    return this._identifier
  }
  public get items(): CartItem[] {
    return this._items
  }
  public get couponCode(): CouponCode | null {
    return this._couponCode
  }
  public get createdAt(): Date {
    return this._createdAt
  }
  public get lastUpdatedAt(): Date {
    return this._lastUpdatedAt
  }
  public get frozen(): boolean {
    return this._frozen
  }
  public applyCouponCode(couponCode: CouponCode): void {
    if (!_.isNil(this._couponCode)) {
            // it depends if we need to allow overriding the coupon code or throw error. purely depends on the business rules
      throw InvalidDataException.dueTo(`There is aleady a coupon code applied to this cart with identifier - ${this._identifier.value}`)
    }
    if (this._items.length === 0) {
      throw InvalidDataException.dueTo('Please add items to the cart before applying the discount code')
    }
    this._couponCode = couponCode
    const totalBeforeDiscount = this.totalAmountForItems()
    const totalAfterDiscount = this.totalAmountAfterApplyingDiscountIfAny()
    this.recordUpdateEvent()
        // record the coupon code applied event
    this.when(
            new CouponCodeApplied(
                this._identifier,
                this._couponCode.label,
                this._couponCode.percentageValue,
                totalBeforeDiscount,
                totalAfterDiscount,
                new Date()
            )
        )
  }
  public totalAmountAfterApplyingDiscountIfAny(): number {
    const amount = this.totalAmountForItems()
    const discount = this.discountToBeApplied(amount)
        // PS: discount should not exceed the actual value ;)
    return amount - discount
  }
  public totalAmountForItems(): number {
    const amount = _.reduce(this.items, (total, item) => {
      return total + item.amount
    },                      0)
    return amount
  }
  public discountToBeApplied(totalValue: number) {
    let discount = 0
    if (!_.isNil(this._couponCode)) {
      const perc = this._couponCode.percentageValue
      discount = (perc * totalValue) / 100
    }
    return discount
  }
  private recordUpdateEvent() {
    this.when(
            new CartUpdated(
                this._identifier,
                this._items,
                this._couponCode,
                this._createdAt,
                this._lastUpdatedAt,
                this._frozen
            )
        )
  }
  public addItem(item: Item, quantity: number): void {
        // since we dont want to add anything after the checkout
    if (this._frozen) {
      return
    }
    const index = _.findLastIndex(this._items, obj => obj.item.identifier.value === item.identifier.value)
    if (index === -1) {
            // item was not found in the cart
      const cartItem = CartItem.create(item, quantity)
      this.items.push(cartItem)
    } else {
            // item exists in the cart, so update its quantity
      const cartItem = this._items[index]
      cartItem.inc(quantity)
      this._items[index] = cartItem
      if (cartItem.quantity <= 0) {
                // remove the item altogether if the quantity is less than 0
        this._items.splice(index, 1)
      }
    }
    this._lastUpdatedAt = new Date()
    this.recordUpdateEvent()
  }
  public removeItem(item: Item, quantity: number) {
        // since we dont want to remove anything after the checkout
    if (this._frozen) {
      return
    }
    const index = _.findIndex(this._items, obj => obj.item.identifier.value === item.identifier.value)

    if (index === -1) {
            // item was not found in the cart; return
      return
    }  {
            // item exists in the cart, so update its quantity
      const cartItem = this._items[index]
      cartItem.dec(quantity)
      this._items[index] = cartItem
      if (cartItem.quantity <= 0) {
                // remove the item altogether if the quantity is less than 0
        this._items.splice(index, 1)
      }
    }
    this._lastUpdatedAt = new Date()
    this.recordUpdateEvent()
  }
  public empty() {
        // reset the items array and
    this._items = []
    this._couponCode = null
    this._lastUpdatedAt = new Date()
    this.recordUpdateEvent()
  }
  public checkout() {
    this._frozen = true
    Object.freeze(this._items)
    this._lastUpdatedAt = new Date()
        // record the cart checkout event
    this.when(
            new CheckoutPerformed(
                this._identifier,
                this._lastUpdatedAt
            )
        )
  }

    // completion events to be handled from the bus for any logging
  protected async whenCartUpdated(event: CartUpdated): Promise<void> {
    console.log('Successfully updated the cart and persisted')
  }
  protected async whenCouponcodeApplied(event: CouponCodeApplied) : Promise<void> {
    console.log('Successfully applied the coupon code to the cart')
  }
  protected async whenCheckoutPerformed(event : CheckoutPerformed) : Promise<void> {
    console.log('Successfully checked out the cart')
  }

  public unmarshall() : any {
    return {
      identifier : this._identifier.value,
      items : _.map(this._items, (item) => {
        return item.deserialize()
      }),
      totalAmount : this.totalAmountForItems(),
      finalAmount : this.totalAmountAfterApplyingDiscountIfAny(),
      couponCode : this._couponCode ? {
        code : this._couponCode.label ,
        value : this._couponCode.percentageValue
      } : '',
      createdAt : this._createdAt.toISOString(),
      lastUpdatedAt : this._lastUpdatedAt.toISOString(),
      hasCheckedOut : this._frozen
    }
  }
}
export { Cart }
