import { ItemIdentifier } from '@domain/value-objects/ItemIdentifier'
import { Item } from '@domain/value-objects/Item'
abstract class ItemRepository {

  abstract get(identifier: ItemIdentifier): Item | null
  abstract exist(identifier: ItemIdentifier): boolean
}

export { ItemRepository }
