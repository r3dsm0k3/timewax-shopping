import { CouponCode } from '@domain/value-objects/CouponCode'

abstract class CouponCodeRepository {

  abstract get(label: string): CouponCode | null
  abstract exist(label: string): boolean
}

export { CouponCodeRepository }
