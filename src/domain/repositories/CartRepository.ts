import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { Cart } from '@domain/aggregate-roots/Cart'

abstract class CartRepository {

  abstract get(identifier: CartIdentifier): Cart | null
  abstract exist(identifier: CartIdentifier): boolean
  abstract save(cart: Cart): Promise<Cart>

  // finders
  abstract find() : Cart[]
}

export { CartRepository }
