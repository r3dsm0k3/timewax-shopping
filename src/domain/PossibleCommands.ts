enum PossibleCommands  {
    CREATE_CART = 'CreateCart',
    ADD_ITEM_TO_CART = 'AddItemToCart',
    REMOVE_ITEM_FROM_CART = 'RemoveItemFromCart',
    EMPTY_CART = 'EmptyCart',
    APPLY_COUPONCODE_TO_CART = 'ApplyCouponCodeToCart',
    CHECKOUT_CART = 'CheckoutCart'
}
export { PossibleCommands }
