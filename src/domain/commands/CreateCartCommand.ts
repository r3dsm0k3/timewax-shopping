import { Command } from '@node-ts/bus-messages'
import { PossibleCommands } from '@domain/PossibleCommands'

class CreateCartCommand extends Command {
  $name = PossibleCommands.CREATE_CART
  $version = 1
  constructor(
    ) {
    super()
  }
}

export { CreateCartCommand }
