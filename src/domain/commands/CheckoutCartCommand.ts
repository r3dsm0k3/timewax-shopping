import { Command } from '@node-ts/bus-messages'
import { PossibleCommands } from '@domain/PossibleCommands'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'

class CheckoutCartCommand extends Command {
  $name = PossibleCommands.CHECKOUT_CART
  $version = 1
  constructor(
        readonly identifier: CartIdentifier
    ) {
    super()
  }
}

export { CheckoutCartCommand }
