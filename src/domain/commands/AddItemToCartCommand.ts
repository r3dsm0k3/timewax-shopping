import { Command } from '@node-ts/bus-messages'
import { PossibleCommands } from '@domain/PossibleCommands'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { ItemIdentifier } from '@domain/value-objects/ItemIdentifier'

class AddItemToCartCommand extends Command {
  $name = PossibleCommands.ADD_ITEM_TO_CART
  $version = 1
  constructor(
        readonly identifier: CartIdentifier,
        readonly itemIdentifier : ItemIdentifier,
        readonly quantity : number
    ) {
    super()
  }
}

export { AddItemToCartCommand }
