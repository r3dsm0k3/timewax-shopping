import { Command } from '@node-ts/bus-messages'
import { PossibleCommands } from '@domain/PossibleCommands'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'

class EmptyCartCommand extends Command {
  $name = PossibleCommands.EMPTY_CART
  $version = 1
  constructor(
        readonly identifier: CartIdentifier
    ) {
    super()
  }
}

export { EmptyCartCommand }
