import { Command } from '@node-ts/bus-messages'
import { PossibleCommands } from '@domain/PossibleCommands'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'

class ApplyCouponCodeToCartCommand extends Command {
  $name = PossibleCommands.APPLY_COUPONCODE_TO_CART
  $version = 1
  constructor(
        readonly identifier: CartIdentifier,
        readonly couponCodeLabel : string
    ) {
    super()
  }
}

export { ApplyCouponCodeToCartCommand }
