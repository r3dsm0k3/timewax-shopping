import { Validator } from '../Validator'
import { InvalidCartIdentifierException } from '../exceptions/InvalidCartIdentifierException'

class CartIdentifier {

  private constructor(private _value: string) { }

  public static with(identifier: string): CartIdentifier {
    Validator.nonEmptyString(identifier, InvalidCartIdentifierException.with(identifier))
    return new this(identifier)
  }

  public get value() : string {
    return this._value
  }
}

export { CartIdentifier }
