import { Item } from '../value-objects/Item'
import { InvalidDataException } from '@domain/exceptions/InvalidDataException'

class CartItem {
  private constructor(
        private readonly _item: Item,
        private _quantity: number
    ) {
  }

  public static create(item: Item, quantity?: number) : CartItem {
    const currentCount = (quantity !== undefined && quantity > 1) ? quantity : 1
    return new this(item, currentCount)
  }
  public get item() : Item {
    return this._item
  }
  public get quantity() : number {
    return this._quantity
  }
  public inc(by: number) {
    // business invariant : items should be between 1-1000
    if (this._quantity + by > 1000) {
      throw InvalidDataException.dueTo('Items quantity should be less than 1000')
    }
    this._quantity += by
  }
  public dec(by: number) {
    if (this._quantity - by < 0) {
      throw InvalidDataException.dueTo('Items quantity should be more than zero.')
    }
    this._quantity -= by
  }
  public get amount() : number {
    return this._item.price * this._quantity
  }
  public deserialize() : any {
    return {
      identifier : this._item.identifier.value,
      label : this._item.label,
      price : this._item.price,
      quantity : this._quantity
    }
  }

}
export { CartItem }
