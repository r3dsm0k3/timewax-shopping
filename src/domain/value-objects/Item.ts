import { ItemIdentifier } from './ItemIdentifier'
import { Validator } from '../Validator'

class Item {
  private constructor(
        private readonly _identifier: ItemIdentifier,
        private readonly _label: string,
        private readonly _price : number
    ) {
  }

  public static with(identifier: ItemIdentifier, label: string, price: number) : Item {
    // since we are not creating value objects for label and price right now, validate here
    Validator.nonEmptyString(label, `Item label should be a non empty string, received ${label}`)
    Validator.positiveInteger(price, `Item price should be a positive integer, received ${price}`)
    return new this(identifier, label, price)
  }
  public get identifier() : ItemIdentifier {
    return this._identifier
  }
  public get label() : string {
    return this._label
  }
  public get price() : number {
    return this._price
  }
}
export { Item }
