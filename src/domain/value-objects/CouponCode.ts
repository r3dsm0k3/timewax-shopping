import { Validator } from '../Validator'
class CouponCode {
  private constructor(
        private readonly _label: string,
        private readonly _value : number
    ) {
  }

  public static with(label: string, value: number) : CouponCode {
    Validator.nonEmptyString(label, `Couponcode label should be a non empty string, received ${label}`)
    Validator.positiveInteger(value, `Couponcode value should be a positive integer, received ${value}`)
    return new this(label, value)
  }
  public get label() : string {
    return this._label
  }
  public get percentageValue() : number {
    return this._value
  }
}
export { CouponCode }
