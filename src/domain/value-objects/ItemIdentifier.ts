import { Validator } from '../Validator'
import { InvalidItemIdentifierException } from '../exceptions/InvalidItemIdentifierException'

class ItemIdentifier {
  private constructor(private _value: string) { }

  public static with(identifier: string): ItemIdentifier {
    Validator.nonEmptyString(identifier, InvalidItemIdentifierException.with(identifier))
    return new this(identifier)
  }

  public get value() : string {
    return this._value
  }
}

export { ItemIdentifier }
