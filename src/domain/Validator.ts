import { Validator as ClassValidator } from 'class-validator'
import { InvalidDataException } from './exceptions/InvalidDataException'

// class validator abstraction for easy validation
class Validator {

  static nonEmptyString(data: any, message? : Error | string) {
    const validator = new ClassValidator()
    if (!validator.isString(data) || validator.isEmpty(data)) {
      Validator.throwFaulty(message)
    }
  }
  static positiveInteger(data: any, message? : Error | string) {
    const validator = new ClassValidator()
    if (!validator.isNumber(data) || validator.isNegative(data)) {
      Validator.throwFaulty(message)
    }
  }
  private static throwFaulty(message: string | Error) {

    if (message instanceof Error) {
      throw message
    }

    throw InvalidDataException.dueTo(message)
  }

}
export { Validator }
