enum PossibleEvents {
    CART_UPDATED = 'timewax/shopping-cart/cart-updated',
    CHECKOUT_PERFORMED = 'timewax/shopping-cart/checkout-performed',
    COUPONCODE_APPLIED = 'timewax/shopping-cart/couponcode-applied'
}
export { PossibleEvents }
