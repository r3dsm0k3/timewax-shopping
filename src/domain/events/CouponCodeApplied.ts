import { Event } from '@node-ts/bus-messages'
import { PossibleEvents } from '../PossibleEvents'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
class CouponCodeApplied extends Event {
  static readonly NAME = PossibleEvents.COUPONCODE_APPLIED
  $name = CouponCodeApplied.NAME
  $version = 0

  constructor (
    readonly cartIdentifier : CartIdentifier,
    readonly couponCodeLabel: string,
    readonly couponCodePercentageValue: number,
    readonly totalCartAmountBeforeApplyingCouponCode : number,
    readonly totalCartAmountAfterApplyingCouponCode : number,
    readonly appliedAt: Date
  ) {
    super()
  }
}
export { CouponCodeApplied }
