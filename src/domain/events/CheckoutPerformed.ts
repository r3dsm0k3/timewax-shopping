import { Event } from '@node-ts/bus-messages'
import { PossibleEvents } from '../PossibleEvents'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
class CheckoutPerformed extends Event {
  static readonly NAME = PossibleEvents.CHECKOUT_PERFORMED
  $name = CheckoutPerformed.NAME
  $version = 0

  constructor (
    readonly cartIdentifier : CartIdentifier,
    // keeping it simple right now.
    // for sure we can add more things like checkout user,amount, shipping cost, any metadata etc here for analytical purposes
    readonly checkedOutAt: Date
  ) {
    super()
  }
}
export { CheckoutPerformed }
