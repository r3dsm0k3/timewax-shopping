import { Event } from '@node-ts/bus-messages'
import { PossibleEvents } from '../PossibleEvents'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { CartItem } from '@domain/value-objects/CartItem'
import { CouponCode } from '@domain/value-objects/CouponCode'
class CartUpdated extends Event {
  static readonly NAME = PossibleEvents.CART_UPDATED
  $name = CartUpdated.NAME
  $version = 0

  constructor (
    readonly cartIdentifier : CartIdentifier,
    readonly items: CartItem[],
    readonly couponCode: CouponCode | null,
    readonly createdAt: Date,
    readonly lastUpdatedAt: Date,
    readonly frozen : boolean
  ) {
    super()
  }
}
export { CartUpdated }
