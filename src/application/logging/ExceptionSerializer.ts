import * as _ from 'lodash'

const exceptionSerializer = (exception: Error) => {
  let previous
  if (_.hasIn(exception, 'previous')) {
    const previousValue = _.get(exception, 'previous')

    if (previousValue instanceof Error) {
      previous = exceptionSerializer(previousValue)
    }
  }

  return {
    previous,
    name: exception.name,
    message: exception.message,
    stack: exception.stack
  }
}

export { exceptionSerializer as ExceptionSerializer }
