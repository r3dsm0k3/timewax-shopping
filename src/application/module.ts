import { ContainerModule, interfaces } from 'inversify'
import { ExceptionSerializer } from './logging/ExceptionSerializer'
import * as pinoNoir from 'pino-noir'
import * as pino from 'pino'
import { config } from '../../config'
import * as applicationInformation from 'pjson'
import { ApplicationBootstrap, BUS_SYMBOLS } from '@node-ts/bus-core'
import { CartCommandHandler } from './handlers/command/CartCommandHandler'
import { CartUpdatedEventHandler } from './handlers/event/CartUpdatedEventHandler'
import { CheckoutPerformedEventHandler } from './handlers/event/CheckoutPerformedEventHandler'
import { CouponCodeAppliedEventHandler } from './handlers/event/CouponCodeAppliedEventHandler'
const applicationContainerModule = new ContainerModule((bind: interfaces.Bind) => {

  bind<pino.Logger>(pino.name)
    .toDynamicValue((context: interfaces.Context) => {
      const noir = pinoNoir(
        {
          serializers : [ExceptionSerializer],
          req: pino.stdSerializers.req,
          res: pino.stdSerializers.res
        },
        ['req.headers.authorization', 'req.headers.x-forwarded-for'],
        '[redacted]')

      const pinoConfig = {
        enabled: config.logger.enabled,
        level: config.logger.level,
        prettyPrint: config.logger.pretty,
        serializers: noir
      }

      return pino(pinoConfig).child({ application: `${applicationInformation.name}@${applicationInformation.version}` })
    })
  bind<ApplicationBootstrap>('Application')
    .toDynamicValue((context: interfaces.Context) => {
      const bootstrap = context.container.get<ApplicationBootstrap>(BUS_SYMBOLS.ApplicationBootstrap)
      bootstrap.registerHandler(CartUpdatedEventHandler)
      bootstrap.registerHandler(CheckoutPerformedEventHandler)
      bootstrap.registerHandler(CouponCodeAppliedEventHandler)
      return bootstrap
    }).inSingletonScope()

  // command binding
  bind<CartCommandHandler>(CartCommandHandler).to(CartCommandHandler)

  // event binding
  bind<CartUpdatedEventHandler>(CartUpdatedEventHandler).to(CartUpdatedEventHandler)
  bind<CheckoutPerformedEventHandler>(CheckoutPerformedEventHandler).to(CheckoutPerformedEventHandler)
  bind<CouponCodeAppliedEventHandler>(CouponCodeAppliedEventHandler).to(CouponCodeAppliedEventHandler)

})
export { applicationContainerModule }
