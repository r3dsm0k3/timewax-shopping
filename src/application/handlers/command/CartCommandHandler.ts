import { injectable, inject } from 'inversify'
import * as pino from 'pino'
import { CartRepository } from '@domain/repositories/CartRepository'
import { ItemRepository } from '@domain/repositories/ItemRepository'
import { CouponCodeRepository } from '@domain/repositories/CouponCodeRepository'
import { AddItemToCartCommand } from '@domain/commands/AddItemToCartCommand'
import { InvalidDataException } from '@domain/exceptions/InvalidDataException'
import * as _ from 'lodash'
import { Cart } from '@domain/aggregate-roots/Cart'
import { ApplyCouponCodeToCartCommand } from '@domain/commands/ApplyCouponCodeToCartCommand'
import { CheckoutCartCommand } from '@domain/commands/CheckoutCartCommand'
import { EmptyCartCommand } from '@domain/commands/EmptyCartCommand'
import { RemoveItemFromCartCommand } from '@domain/commands/RemoveItemFromCartCommand'
import { CreateCartCommand } from '@domain/commands/CreateCartCommand'
@injectable()
class CartCommandHandler {
  constructor(
        @inject(pino.name) private readonly _logger: pino.Logger,
        @inject(CartRepository) private readonly _repo : CartRepository,
        @inject(ItemRepository) private readonly _itemRepo : ItemRepository,
        @inject(CouponCodeRepository) private readonly _couponCodeRepo : CouponCodeRepository,
        )
    {}

  async handleCreateCart(command: CreateCartCommand) {
    try {
      const cart = Cart.create()
      await this._repo.save(cart)
      return cart
    } catch (error) {
      this._logger.error(error)
      throw error
    }
  }
  async handleAddItem(command: AddItemToCartCommand) {
    try {
            // check if the item with the provided identifier exists, if yes, add else throw error
      const item = this._itemRepo.get(command.itemIdentifier)
      if (_.isEmpty(item)) {
        throw InvalidDataException.dueTo(`Provided item with the identifier doesnt exist in the system - ${ command. itemIdentifier. value}`)
      }
      const cartFromDb = this._repo.get(command.identifier)
      if (_.isEmpty(cartFromDb)) {
        throw InvalidDataException.dueTo(`Provided cart with the identifier doesnt exist in the system - ${ command. identifier. value}`)
      }
      // since we are not deleting the cart from the memory db, currently even the frozen carts will show up here
      // but since the addItem handles it anyway, it is alright for now.
      cartFromDb.addItem(item, command.quantity)
      await this._repo.save(cartFromDb)
      return cartFromDb

    } catch (error) {
            // log the error and bubble it upwards
      this._logger.error(error)
      throw error
    }
  }
  async handleRemoveItem(command : RemoveItemFromCartCommand) {
    try {
            // check if the item with the provided identifier exists, if yes, add else throw error
      const item = this._itemRepo.get(command.itemIdentifier)
      if (_.isEmpty(item)) {
        throw InvalidDataException.dueTo(`Provided item with the identifier doesnt exist in the system - ${ command. itemIdentifier. value}`)
      }
            // check if the cart already exists
      const cartFromDb = this._repo.get(command.identifier)
      if (_.isEmpty(cartFromDb)) {
        throw InvalidDataException.dueTo(`Provided cart with the identifier doesnt exist in the system - ${ command. identifier. value}`)
      } else {
                // since we are not deleting the cart from the memory db, currently even the frozen carts will show up here
                // but since the addItem handles it anyway, it is alright for now.
        cartFromDb.removeItem(item, command.quantity)
        await this._repo.save(cartFromDb)
        return cartFromDb
      }
    } catch (error) {
             // log the error and bubble it upwards
      this._logger.error(error)
      throw error
    }
  }
  async handleApplyCouponCode(command : ApplyCouponCodeToCartCommand) {
    try {
      const cartFromdb = this._repo.get(command.identifier)
      if (_.isEmpty(cartFromdb)) {
        throw InvalidDataException.dueTo(`Provided cart with the identifier doesnt exist in the system - ${ command. identifier. value}`)
      }
            // check if the coupon code exists
      const couponCodeFromdb = this._couponCodeRepo.get(command.couponCodeLabel)
      if (_.isEmpty(couponCodeFromdb)) {
        throw InvalidDataException.dueTo(`Provided coupon code with the label doesnt exist in the system - ${ command.couponCodeLabel}`)
      }
      cartFromdb.applyCouponCode(couponCodeFromdb)
      await this._repo.save(cartFromdb)
      return cartFromdb
    } catch (error) {
            // log the error and bubble it upwards
      this._logger.error(error)
      throw error
    }
  }
  async handleCheckout(command : CheckoutCartCommand) {
    try {
      const cartFromdb = this._repo.get(command.identifier)
      if (_.isEmpty(cartFromdb)) {
        throw InvalidDataException.dueTo(`Provided cart with the identifier doesnt exist in the system - ${ command. identifier. value}`)
      }
      cartFromdb.checkout()
      await this._repo.save(cartFromdb)
      return cartFromdb
    } catch (error) {
             // log the error and bubble it upwards
      this._logger.error(error)
      throw error
    }
  }
  async handleReset(command : EmptyCartCommand) {
    try {
      const cartFromdb = this._repo.get(command.identifier)
      if (_.isEmpty(cartFromdb)) {
        throw InvalidDataException.dueTo(`Provided cart with the identifier doesnt exist in the system - ${ command. identifier. value}`)
      }
      cartFromdb.empty()
      await this._repo.save(cartFromdb)
    } catch (error) {
             // log the error and bubble it upwards
      this._logger.error(error)
      throw error
    }
  }
}
export { CartCommandHandler }
