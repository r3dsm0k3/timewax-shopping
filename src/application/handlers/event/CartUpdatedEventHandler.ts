import * as pino from 'pino'
import { injectable, inject } from 'inversify'
import { HandlesMessage } from '@node-ts/bus-core'
import { CartUpdated } from '@domain/events/CartUpdated'

@injectable() @HandlesMessage(CartUpdated)

class CartUpdatedEventHandler {
  constructor(
        @inject(pino.name) private readonly _logger: pino.Logger,
      ) {}

  async handle(event: CartUpdated) : Promise<void> {
    this._logger.info('Successfully received the cart updated event', {
      event : event.$name,
      cartIdentifier : event.cartIdentifier.value,
      at : event.lastUpdatedAt.toISOString()
    })
  }
}
export { CartUpdatedEventHandler }
