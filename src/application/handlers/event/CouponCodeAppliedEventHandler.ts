import * as pino from 'pino'
import { injectable, inject } from 'inversify'
import { HandlesMessage } from '@node-ts/bus-core'
import { CouponCodeApplied } from '@domain/events/CouponCodeApplied'

@injectable() @HandlesMessage(CouponCodeApplied)

class CouponCodeAppliedEventHandler {
  constructor(
        @inject(pino.name) private readonly _logger: pino.Logger,
      ) {}
  async handle(event: CouponCodeApplied) : Promise<void> {
    this._logger.info('Successfully received the coupon code applied event', {
      event : event.$name,
      code : event.couponCodeLabel,
      percentage : event.couponCodePercentageValue,
      cartIdentifier : event.cartIdentifier.value
    })
  }
}
export { CouponCodeAppliedEventHandler }
