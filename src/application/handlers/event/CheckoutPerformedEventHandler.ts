import * as pino from 'pino'
import { injectable, inject } from 'inversify'
import { HandlesMessage } from '@node-ts/bus-core'
import { CheckoutPerformed } from '@domain/events/CheckoutPerformed'

@injectable() @HandlesMessage(CheckoutPerformed)

class CheckoutPerformedEventHandler {
  constructor(
        @inject(pino.name) private readonly _logger: pino.Logger,
      ) {}
  async handle(event: CheckoutPerformed) : Promise<void> {
    this._logger.info('Successfully received the checkout performed event', {
      event : event.$name,
      cartIdentifier : event.cartIdentifier.value
    })
  }
}
export { CheckoutPerformedEventHandler }
