import 'reflect-metadata'
import * as _ from 'lodash'
import * as pino from 'pino'
import { container } from './container'
import { config } from '../config'
import { ApplicationBootstrap } from '@node-ts/bus-core'
import { Server } from 'restify'

const run = async () => {

  // setup logging
  const rootLogger = container.get<pino.Logger>(pino.name)
  try {

    // log the irrecoverable process exceptions.
    // we should not try to recover from errors here.
    process.on('exit', (code) => {
      rootLogger.info(`Process is about to exit with code ${code}`)
    })
    process.on('uncaughtException', (e) => {
      rootLogger.error(
        {
          exception: e
        },
        'Uncaught exception occured'
      )
    })
    process.on('unhandledRejection', (reason : any, e) => {
      rootLogger.error(
        {
          reason,
          stack: reason.stack,
          exception: e,
        },
        'Unhandled rejection occured'
      )
    })

    rootLogger.info('initializing application state...')
    const bootstrap = container.get<ApplicationBootstrap>('Application')
    await bootstrap.initialize(container)

    rootLogger.info('Starting HTTP server...')
    const server = container.get<Server>('HttpServer')
    const port = config.server.port
    server.listen(port, () => {
      rootLogger.info(`🚀 Started HTTP Server on port ${port}. Listening...`)
    })

  } catch (e) {
    rootLogger.error(
      {
        exception: e
      },
      'Exception occured during start'
    )
  }

}
run()
