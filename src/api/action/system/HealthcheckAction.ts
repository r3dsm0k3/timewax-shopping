import { Action } from '../../Action'
import { Request, Response, Next } from 'restify'
import { Controller, Get } from 'inversify-restify-utils'
import { injectable } from 'inversify'

@injectable()
@Controller('/system')
class HealthcheckAction implements Action {

  @Get('/health')
  public async execute(req: Request, res: Response, next: Next): Promise<void> {
    res.send(204)
    next()
  }
}

export { HealthcheckAction }
