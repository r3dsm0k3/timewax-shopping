import { Action } from '../../Action'
import { Request, Response, Next } from 'restify'
import { Controller, Get } from 'inversify-restify-utils'
import { injectable, inject } from 'inversify'
import * as pino from 'pino'
import { CartRepository } from '@domain/repositories/CartRepository'
import * as _ from 'lodash'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'

@injectable()
@Controller('/cart')
class GetCartByIdentifierAction implements Action {
  constructor(
    @inject(pino.name) private readonly _logger : pino.Logger,
    @inject(CartRepository) private readonly _repo : CartRepository
) {}

  @Get('/:cart_id')
  public async execute(req: Request, res: Response, next: Next): Promise<void> {
    try {
      const identifier =  CartIdentifier.with(req.params.cart_id)
      const cart = this._repo.get(identifier)
      res.json(200, { status : true, data : cart.unmarshall() })
    } catch (error) {
      // todo: better error handing here
      res.json(422, { status : false , reason : error.message })
      this._logger.error({
        exception: error
      })
    }
    next()
  }
}

export { GetCartByIdentifierAction }
