import { Action } from '../../Action'
import { Request, Response, Next } from 'restify'
import { Controller, Post } from 'inversify-restify-utils'
import { injectable, inject } from 'inversify'
import * as pino from 'pino'
import * as _ from 'lodash'
import { CartCommandHandler } from '@application/handlers/command/CartCommandHandler'
import { AddItemToCartCommand } from '@domain/commands/AddItemToCartCommand'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { ItemIdentifier } from '@domain/value-objects/ItemIdentifier'

@injectable()
@Controller('/cart')
class AddItemToCartAction implements Action {
  constructor(
    @inject(pino.name) private readonly _logger : pino.Logger,
    @inject(CartCommandHandler) private _handler : CartCommandHandler
) {}

  @Post('/:cart_id/item')
  public async execute(req: Request, res: Response, next: Next): Promise<void> {
    try {
      const cartIdentifier = CartIdentifier.with(req.params.cart_id)
      const itemIdentifier = ItemIdentifier.with(req.body.item_identifier)
      const quantity = req.body.quantity
      const command = new AddItemToCartCommand(cartIdentifier, itemIdentifier, quantity)
      const cart = await this._handler.handleAddItem(command)
      res.json(200, { status : true, data : cart.unmarshall() })
    } catch (error) {
      // better error handing here
      res.json(422, { status : false , reason : error.message })
      this._logger.error({
        exception: error
      })
    }
    next()
  }
}

export { AddItemToCartAction }
