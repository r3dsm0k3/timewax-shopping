import { Action } from '../../Action'
import { Request, Response, Next } from 'restify'
import { Controller, Post, Delete, Put } from 'inversify-restify-utils'
import { injectable, inject } from 'inversify'
import * as pino from 'pino'
import * as _ from 'lodash'
import { CartCommandHandler } from '@application/handlers/command/CartCommandHandler'
import { Cart } from '@domain/aggregate-roots/Cart'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { ApplyCouponCodeToCartCommand } from '@domain/commands/ApplyCouponCodeToCartCommand'

@injectable()
@Controller('/cart')
class ApplyCouponCodeAction implements Action {
  constructor(
    @inject(pino.name) private readonly _logger : pino.Logger,
    @inject(CartCommandHandler) private _handler : CartCommandHandler
) {}

  @Put('/:cart_id/coupon')
  public async execute(req: Request, res: Response, next: Next): Promise<void> {
    try {
      const cartIdentifier = CartIdentifier.with(req.params.cart_id)
      const couponCode = req.body.coupon_code
      const command = new ApplyCouponCodeToCartCommand(cartIdentifier, couponCode)
      const cart = await this._handler.handleApplyCouponCode(command)
      res.json(200, { status : true, data : cart.unmarshall() })
    } catch (error) {
      // better error handing here
      res.json(422, { status : false , reason : error.message })
      this._logger.error({
        exception: error
      })
    }
    next()
  }
}

export { ApplyCouponCodeAction }
