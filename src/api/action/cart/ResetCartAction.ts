import { Action } from '../../Action'
import { Request, Response, Next } from 'restify'
import { Controller, Post, Delete } from 'inversify-restify-utils'
import { injectable, inject } from 'inversify'
import * as pino from 'pino'
import * as _ from 'lodash'
import { CartCommandHandler } from '@application/handlers/command/CartCommandHandler'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { EmptyCartCommand } from '@domain/commands/EmptyCartCommand'

@injectable()
@Controller('/cart')
class ResetCartAction implements Action {
  constructor(
    @inject(pino.name) private readonly _logger : pino.Logger,
    @inject(CartCommandHandler) private _handler : CartCommandHandler
) {}

  @Delete('/:cart_id')
  public async execute(req: Request, res: Response, next: Next): Promise<void> {
    try {
      const cartIdentifier = CartIdentifier.with(req.params.cart_id)
      const command = new EmptyCartCommand(cartIdentifier)
      await this._handler.handleReset(command)
      res.json(200, { status : true })
    } catch (error) {
      // better error handing here
      res.json(422, { status : false , reason : error.message })
      this._logger.error({
        exception: error
      })
    }
    next()
  }
}

export { ResetCartAction }
