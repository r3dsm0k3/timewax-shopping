import { Action } from '../../Action'
import { Request, Response, Next } from 'restify'
import { Controller, Get } from 'inversify-restify-utils'
import { injectable, inject } from 'inversify'
import * as pino from 'pino'
import { CartRepository } from '@domain/repositories/CartRepository'
import { Cart } from '@domain/aggregate-roots/Cart'
import * as _ from 'lodash'

@injectable()
@Controller('/cart')
class GetCartsAction implements Action {
  constructor(
    @inject(pino.name) private readonly _logger : pino.Logger,
    @inject(CartRepository) private readonly _repo : CartRepository
) {}

  @Get('/')
  public async execute(req: Request, res: Response, next: Next): Promise<void> {
    try {
      const carts = this._repo.find()
      res.json(200, { status : true, data : this.marshalToJson(carts) })
    } catch (error) {
      // better error handing here
      res.json(422, { status : false , reason : error.message })
      this._logger.error({
        exception: error
      })
    }
    next()
  }

  private marshalToJson(cartItems : Cart[]) {
    let arr = []
    arr = _.map(cartItems, (cart) => {
      return cart.unmarshall()
    })
    return {
      carts : arr
    }
  }
}

export { GetCartsAction }
