import { Action } from '../../Action'
import { Request, Response, Next } from 'restify'
import { Controller, Post } from 'inversify-restify-utils'
import { injectable, inject } from 'inversify'
import * as pino from 'pino'
import * as _ from 'lodash'
import { CartCommandHandler } from '@application/handlers/command/CartCommandHandler'
import { CreateCartCommand } from '@domain/commands/CreateCartCommand'
import { Cart } from '@domain/aggregate-roots/Cart'

@injectable()
@Controller('/cart')
class CreateCartAction implements Action {
  constructor(
    @inject(pino.name) private readonly _logger : pino.Logger,
    @inject(CartCommandHandler) private _handler : CartCommandHandler
) {}

  @Post('/')
  public async execute(req: Request, res: Response, next: Next): Promise<void> {
    try {
      const command = new CreateCartCommand()
      const cart = await this._handler.handleCreateCart(command)
      res.json(200, { status : true, data : cart.unmarshall() })
    } catch (error) {
      // better error handing here
      res.json(422, { status : false , reason : error.message })
      this._logger.error({
        exception: error
      })
    }
    next()
  }
}

export { CreateCartAction }
