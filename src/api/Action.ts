import { Request, Response, Next } from 'restify'
import { injectable } from 'inversify'

@injectable()
abstract class Action {
  public abstract execute(req: Request, res: Response, next: Next): Promise<void>
}

export { Action }
