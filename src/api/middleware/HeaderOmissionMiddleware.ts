import { Request, Response, Next } from 'restify'

const headerOmissionMiddleware = (...headersToBeOmitted: string[]) => {
  return (req: Request, res: Response, next: Next) => {
    headersToBeOmitted.forEach((header) => {
      res.removeHeader(header)
    })

    next()
  }
}

export { headerOmissionMiddleware as HeaderOmissionMiddleware }
