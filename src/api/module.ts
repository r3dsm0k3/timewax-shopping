import { ContainerModule, interfaces } from 'inversify'
import { InversifyRestifyServer, TYPE, interfaces as restifyInterfaces } from 'inversify-restify-utils'
import { HeaderOmissionMiddleware } from './middleware/HeaderOmissionMiddleware'
import { Server, plugins } from 'restify'
import * as applicationInformation from 'pjson'
import * as pino from 'pino'
import * as restifyPinoLogger from 'restify-pino-logger'
import { HealthcheckAction } from './action/system/HealthcheckAction'
import { CreateCartAction } from './action/cart/CreateCartAction'
import { GetCartsAction } from './action/cart/GetCartsAction'
import { GetCartByIdentifierAction } from './action/cart/GetCartByIdentifierAction'
import { AddItemToCartAction } from './action/cart/AddItemToCartAction'
import { RemoveItemFromCartAction } from './action/cart/RemoveItemFromCartAction'
import { ApplyCouponCodeAction } from './action/cart/ApplyCouponCodeAction'
import { CheckoutCartAction } from './action/cart/CheckoutCartAction'
import { ResetCartAction } from './action/cart/ResetCartAction'

const apiContainerModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<Server>('HttpServer')
    .toDynamicValue((context: interfaces.Context) => {
      const container = context.container
      const rootLogger = container.get<pino>(pino.name)
      const server = new InversifyRestifyServer(container, { name: `${applicationInformation.name}@${applicationInformation.version}` })
      server.setConfig((app) => {
        app.use(plugins.bodyParser())
        app.use(restifyPinoLogger({ logger: rootLogger }))
        app.use(HeaderOmissionMiddleware('server'))
      })
      return server.build()
    })

  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(HealthcheckAction)
    .whenTargetNamed('HealthcheckAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(CreateCartAction)
    .whenTargetNamed('CreateCartAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(GetCartsAction)
    .whenTargetNamed('GetCartsAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(GetCartByIdentifierAction)
    .whenTargetNamed('GetCartByIdentifierAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(AddItemToCartAction)
    .whenTargetNamed('AddItemToCartAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(RemoveItemFromCartAction)
    .whenTargetNamed('RemoveItemFromCartAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(ApplyCouponCodeAction)
    .whenTargetNamed('ApplyCouponCodeAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(CheckoutCartAction)
    .whenTargetNamed('CheckoutCartAction')
  bind<restifyInterfaces.Controller>(TYPE.Controller)
    .to(ResetCartAction)
    .whenTargetNamed('ResetCartAction')
})
export { apiContainerModule }
