import { inject, injectable } from 'inversify'
import { ItemType } from './ItemType'
import * as _ from 'lodash'
import { Item } from '@domain/value-objects/Item'
import { ItemIdentifier } from '@domain/value-objects/ItemIdentifier'
import { ItemRepository } from '@domain/repositories/ItemRepository'
@injectable()
class InMemoryBackedItemRepository implements ItemRepository {
  constructor(
        @inject('Items') private readonly _items: ItemType[],
        ) {
  }
  public get(identifier: ItemIdentifier): Item | null {
    const index = _.findLastIndex(this._items, item => item.identifier === identifier.value)
    if (index !== -1) {
      const foundItem = this._items[index]
      const item = Item.with(
          ItemIdentifier.with(foundItem.identifier),
          foundItem.label,
          foundItem.price
        )
      return item
    }
    return null
  }
  public exist(identifier: ItemIdentifier): boolean {
    const index = _.findLastIndex(this._items, item => item.identifier === identifier.value)
    return index >= 0
  }
}
export { InMemoryBackedItemRepository }
