import { AbstractEventEmittingRepository } from './AbstractEventEmittingRepository'
import { CartRepository } from '@domain/repositories/CartRepository'
import { CartIdentifier } from '@domain/value-objects/CartIdentifier'
import { Cart } from '@domain/aggregate-roots/Cart'
import { injectable, inject } from 'inversify'
import { BUS_SYMBOLS, Bus } from '@node-ts/bus-core'
import * as pino from 'pino'
import * as _ from 'lodash'

@injectable()
class InMemoryBackedCartRepository extends AbstractEventEmittingRepository implements CartRepository {
  private _db : Cart[] = []
  constructor(
        @inject(BUS_SYMBOLS.Bus) private readonly bus: Bus,
        @inject(pino.name) private readonly logger : pino.Logger
    ) {
    super(bus, logger)
  }

  public get(identifier: CartIdentifier): Cart {
    const index = _.findLastIndex(this._db, cart => cart.identifier.value === identifier.value)
    if (index !== -1) {
      const foundCart = this._db[index]
      return foundCart
    }
    return null
  }
  public exist(identifier: CartIdentifier): boolean {
    const index = _.findLastIndex(this._db, cart => cart.identifier.value === identifier.value)
    return index >= 0
  }
  public async save(cart: Cart): Promise<Cart> {
        // check if the same cart exists in the db and if it is an update operation
    const index = _.findLastIndex(this._db, cart => cart.identifier.value === cart.identifier.value)
    if (index !== -1) {
            // reset the cart to the new one.
      this._db[index] = cart
    } else {
      this._db.push(cart)
    }
        // publish all the events saved for the cart
    await this.publishChanges(cart)
        // return the saved
    return cart
  }
  find(): Cart[] {
    return this._db
  }

}
export { InMemoryBackedCartRepository }
