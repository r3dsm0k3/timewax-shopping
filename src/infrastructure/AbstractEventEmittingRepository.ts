import { unmanaged, injectable } from 'inversify'
import * as pino from 'pino'
import { AggregateRoot } from '@node-ts/ddd'
import { Bus } from '@node-ts/bus-core'
@injectable()
abstract class AbstractEventEmittingRepository {
  constructor (
      @unmanaged() private readonly _bus: Bus,
      @unmanaged() protected readonly _logger: pino.Logger
    ) {
  }
    /**
   * Publish and clear the Aggregate Root's changes
   */
  protected async publishChanges (root: AggregateRoot): Promise<void> {
    const publishPromises = root.changes.map(event => this._bus.publish(event))
    await Promise.all(publishPromises)
    root.clearChanges()
  }
}
export { AbstractEventEmittingRepository }
