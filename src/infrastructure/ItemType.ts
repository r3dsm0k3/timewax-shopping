interface ItemType {
  identifier : string,
  label : string,
  price : number
}
// ideally the item identifiers should be a uuid or something, added enumeratable numbers for simplicity
const items : ItemType[] = [
  {
    identifier : '1',
    label : 'The first item',
    price : 100
  },
  {
    identifier : '2',
    label : 'The second item',
    price : 200
  },
  {
    identifier : '3',
    label : 'The thrid item',
    price : 300
  },
  {
    identifier : '4',
    label : 'The fourth item',
    price : 400
  },
  {
    identifier : '5',
    label : 'The fifth item',
    price : 500
  },
  {
    identifier : '6',
    label : 'The sixth item',
    price : 600
  },
  {
    identifier : '7',
    label : 'The seventh item',
    price : 700
  },
  {
    identifier : '1337',
    label : 'The 1337 item',
    price : 1337
  }

]
export { ItemType , items }
