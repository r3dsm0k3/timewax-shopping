import { CouponCodeRepository } from '@domain/repositories/CouponCodeRepository'
import { CouponCode } from '@domain/value-objects/CouponCode'
import { inject , injectable } from 'inversify'
import { CouponCodeType } from './CouponCodeType'
import * as _ from 'lodash'
@injectable()
class InMemoryBackedCouponCodeRepository implements CouponCodeRepository {
  constructor(
        @inject('Coupons') private readonly _coupons: CouponCodeType[],
        ) {
  }
  public get(label: string): CouponCode | null {
    const index = _.findLastIndex(this._coupons, coupon => coupon.label === label)
    if (index !== -1) {
      const foundCoupon = this._coupons[index]
      const coupon = CouponCode.with(
          foundCoupon.label,
          foundCoupon.val
        )
      return coupon
    }
    return null
  }
  public exist(label: string): boolean {
    const index = _.findLastIndex(this._coupons, coupon => coupon.label === label)
    return index >= 0
  }
}
export { InMemoryBackedCouponCodeRepository }
