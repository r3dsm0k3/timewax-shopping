import { ContainerModule, interfaces } from 'inversify'
import { CartRepository } from '@domain/repositories/CartRepository'
import { InMemoryBackedCartRepository } from './InMemoryBackedCartRepository'
import { CouponCodeRepository } from '@domain/repositories/CouponCodeRepository'
import { InMemoryBackedCouponCodeRepository } from './InMemoryBackedCouponCodeRepository'
import { ItemRepository } from '@domain/repositories/ItemRepository'
import { InMemoryBackedItemRepository } from './InMemoryBackedItemRepository'
import { coupons, CouponCodeType } from './CouponCodeType'
import { ItemType, items } from './ItemType'

const infrastructureContainerModule = new ContainerModule((bind: interfaces.Bind) => {

    // repositories
  bind<CartRepository>(CartRepository)
    .to(InMemoryBackedCartRepository).inSingletonScope()
  bind<CouponCodeRepository>(CouponCodeRepository)
    .to(InMemoryBackedCouponCodeRepository).inSingletonScope()
  bind<ItemRepository>(ItemRepository)
    .to(InMemoryBackedItemRepository).inSingletonScope()

    // bootstrap data for items, coupons
  bind<ItemType[]>('Items').toConstantValue(items)
  bind<CouponCodeType[]>('Coupons').toConstantValue(coupons)

})
export { infrastructureContainerModule }
