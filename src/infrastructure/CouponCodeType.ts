interface CouponCodeType {
  label : string,
  val : number
}
const coupons : CouponCodeType[] = [
  {
    label : 'TimeWax1',
    val : 10
  },
  {
    label : 'TimeWax2',
    val : 10
  },
  {
    label : 'TimeWax3',
    val : 10
  },
]
export { CouponCodeType, coupons }
