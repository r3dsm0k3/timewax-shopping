import { Container } from 'inversify'
import { apiContainerModule } from '@api/module'
import { applicationContainerModule } from '@application/module'
import { infrastructureContainerModule } from '@infrastructure/module'
import { BusModule } from '@node-ts/bus-core'
import { LoggerModule } from '@node-ts/logger-core'
import { PinoModule } from '@node-ts/logger-pino'

const container = new Container()
container.load(
  apiContainerModule,
  applicationContainerModule,
  infrastructureContainerModule,
  new BusModule(),
  new LoggerModule(),
  // ps : I authored the pino module for node-ts :)
  new PinoModule()
)
export { container }
