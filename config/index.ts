const configs = require('./configs')
const secrets = require('./secrets')
const _ = require('lodash')

interface ConfigType {
  server: {
    port: number,
    url: string,
    enableGzip: boolean
  },
  logger: {
    enabled: boolean,
    level: string,
    pretty: boolean
  }
}

const config: ConfigType = _.merge({}, configs, secrets)

export { config }
